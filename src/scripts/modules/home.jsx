import React from 'react'
import DocumentTitle from 'react-document-title'

export default React.createClass({
  render() {
    return (
      <DocumentTitle title="Homepage">
        <main>
          <section className="main_content">
            <p>Welcome to the homepage.</p>
          </section>
        </main>
      </DocumentTitle>
    )
  }
})
