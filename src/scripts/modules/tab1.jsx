import React from 'react'
import DocumentTitle from 'react-document-title'

export default React.createClass({
  render() {
    return (
      <DocumentTitle title="First Tab | Page 1">
        <section className="main_content">
          <p>First Tab</p>
        </section>
      </DocumentTitle>
    )
  }
})
