import React from 'react'
import DocumentTitle from 'react-document-title'

export default React.createClass({
  render() {
    return (
      <DocumentTitle title="Page 2">
        <main>
          <section className="main_content">
            <p>Page 2</p>
          </section>
        </main>
      </DocumentTitle>
    )
  }
})
