import React from 'react'
import DocumentTitle from 'react-document-title'

export default React.createClass({
  render() {
    return (
      <DocumentTitle title="Second Tab | Page 1">
        <section className="main_content">
          <p>Second Tab</p>
        </section>
      </DocumentTitle>
    )
  }
})
